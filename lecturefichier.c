#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "struct.h"
#include "toolbox.h"
#include "create.h"

/* Repère si un caractère d'une chaîne est un chiffre ou non */

int chiffre(int n){
  switch (n){
  case 48: 
    return 1;
    break;
  case 49:
    return 1;
    break;
  case 50:
    return 1;
    break;
  case 51:
    return 1;
    break;
  case 52:
    return 1;
    break;
  case 53:
    return 1;
    break;
  case 54:
    return 1;
    break;
  case 55:
    return 1;
    break;
  case 56:
    return 1;
    break;
  case 57:
    return 1;
    break;
  default:
    return 0;
    break;
  }
}

/* Détermine la longueur d'un nombre à partir de l'emplacement du curseur */

int longueur_nombre(char *str, int curseur){ 
  int i=curseur;
  while(chiffre(str[i]))
      i++;
  return i-curseur;
}

/* Retourne le nombre lu dans le fichier à partir du curseur */
 
int convertir_nombre(char *str, int curseur){
  char nombre[longueur_nombre(str,curseur)];
  int i=curseur;
  int k=0;
  int nb=0;
  while (chiffre(str[i])){
    nombre[k]=str[i];
    i++;
    k++;
  }
  nombre[k]='\0';
  nb=atoi(nombre);
  return nb;
}

/* Fonction qui lit la première ligne du fichier .txt et retourner les dimensions de la grille sous forme de struct */

struct Dimension convertir_DIM(char *str){
  int compteur_nombre=0;
  int curseur=0;
  int longueur;
  struct Dimension dim = {99,99}; 
  while (curseur < strlen(str)){
    if (chiffre(str[curseur])){
      longueur=longueur_nombre(str,curseur);
      if (compteur_nombre == 1){
	dim.nb_col=convertir_nombre(str,curseur);
	compteur_nombre++;
	curseur=curseur+longueur;
      }
      if (compteur_nombre == 0){
	dim.nb_lig=convertir_nombre(str,curseur);
	compteur_nombre++;
	curseur=curseur+longueur;
      }
    }
    else curseur++;
  }
  return dim;
}

/* Compte combien de nombre on a dans une ligne avant le prochain ';'. i.e. détermine longueur d'une séquence */

int compter_nombres(char *str,int curseur){ 
  int compteur=0;
  int i=curseur;
  int longueur;
  while (str[i] != ';'){
    if (chiffre(str[i]) || str[i]=='\n'){ 
      longueur=longueur_nombre(str,i);
      compteur++;
      i=i+longueur;
    }
    if (str[i] == ' ') 
      i++;
    else break; 
  }
  return compteur;
}

/* Compte le nombre de ';' dans une ligne  */

int compter_pointvirg(char *str){
  int compteur=0;
  int curseur=0;
  while (curseur < strlen(str)){
    if (str[curseur]==';')
      compteur++;
    curseur++;
  }
  return compteur;
}

/* Convertie les 2e et 3e ligne du fichier .txt et affecte le tout dans un struct Table */

struct Table convertir_LIGNE(char *str){ // Imposer ';' à la fin de la ligne à convertir 
  struct Table LIGNE;
  int s=0; //nb de sequence
  int curseur=0;
  int n=0; // nb d'element
  int longueur=0;
  while (curseur < strlen(str)){
    if (str[curseur] == ';' || str[curseur] == '\n' || str[curseur] == ' ')
      curseur++;
    else {
      assert(s < strlen(str));
      LIGNE.tab_taille[s]=compter_nombres(str,curseur);
      s++;
      while (str[curseur] != ';'){
	if (chiffre(str[curseur])){
	  assert(n < 1000);
	  LIGNE.tab_elt[n]=convertir_nombre(str,curseur);
	  longueur=longueur_nombre(str,curseur);
	  n++;
	  curseur=curseur+longueur;
	}
	else curseur++;
      }
    }
  }
  LIGNE.nb_seq=s;
  LIGNE.nb_elt=n;
  return LIGNE;
}

/* Insère dans la grille la N-ième ligne à partir de la lecture du fichier .txt */

struct Ensemble inserer_ligne_fichier(char *str,int n, struct Ensemble GRID){ 
  struct Sequence LIGNE;
  int curseur=0;
  LIGNE.t_seq=strlen(str)-1;
  int nb_err=0;
  while ((curseur < strlen(str)-1) && (str[curseur] != ' ')){
    if (alphabet(str[curseur])){
      LIGNE.seq[curseur]=str[curseur]; 
      curseur++;
      }
    else { 
      printf("erreur de syntaxe au %d-ième caractère de la ligne %d \n",curseur,n);
      nb_err++;
      curseur++;
    }
  }
  if (nb_err == 0){
    for (int i=0; i < LIGNE.t_seq;i++){
      GRID.grille.ligne[GRID.dim.nb_col*n+i]=LIGNE.seq[i];
    }
  }
  return GRID;   
}

/* Fonction qui doit lire le fichier et définir les éléments structurels de de la grille à étudier */
/* ATTENTION à bien écrire la grille vide dans le fichier .txt  dans le cas où on veut résoudre une grille avec 0 cases colorier initialement */

struct Ensemble convertir_fichier(char *str){ 
  struct Ensemble init;
  char ligne[5000];
  int c=0;//Numéro de la ligne lue
  int n=0;//Numéro de la ligne dans la grille
  FILE *f = fopen(str,"r");
  if(fgets(ligne,5000,f) != NULL && c==0){
    init.dim=convertir_DIM(ligne);
    c++;
  }

  if(fgets(ligne,5000,f) != NULL && c==1){
    init.seq_ligne=convertir_LIGNE(ligne);
    c++;
  }

  if(fgets(ligne,5000,f) != NULL && c==2){
    init.seq_col=convertir_LIGNE(ligne);
    c++;
  }
  
  while(fgets(ligne,5000,f) != NULL && c >=3){
    init=inserer_ligne_fichier(ligne,n,init);
    c++;
    n++;
  }
  
  init.grille.t_ligne=init.dim.nb_lig*init.dim.nb_col;

  fclose(f);
  return init;
}


