#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "struct.h"
#include "toolbox.h"

/*La fonction contrainte dit si, pour une séquence donnée, il est sera possible de trouver des cases à noircir à coup sûr */

/*Considère qu'il n'y a pas de contrainte si 0 seul élément de la séquence, ce cas sera traité à part*/

int contrainte(struct Asso A){
  struct Sequence S=A.Seq;
  struct Ligne L=A.Line;
  int s=S.t_seq;
  int c=L.t_ligne;
  int maxi=max(S);
  
  int g=0; //nombre d'éléments à gauche du maximum dans la séquence
  struct Sequence droite; 
  struct Sequence gauche; 
  
  while (maxi != S.seq[g]){
    gauche.seq[g]=S.seq[g];
    g++;
  }
  int d=s-(g+1); //nombre d'éléments à droite du maxi dans la séquence
  for(int i=0 ; i < d ; i++){
    droite.seq[i]=S.seq[g+1+i];
  }
  int pocc=2*maxi;
  int pres=c-pocc;
  
  droite.t_seq=d;
  gauche.t_seq=g;
  
  if (pres < minspace(gauche) + minspace(droite)) 
    return 1;
  else return 0;    
  
}

/*Cette fonction colorie à coup sûr des cases en noir dans la ligne dans le cas où une contrainte existe. Si on n'a pas de contrainte la fonction renvoit la même ligne qu'en entrée */

struct Ligne boxe(struct Asso A){ 
  struct Sequence S=A.Seq;
  struct Ligne L=A.Line;
  if (Complete(L) == 1)
    return L;
  else{
    if (minspace(S) > L.t_ligne + 1){ //Cas où la séquence d'entier ne rentre pas dans la ligne
    }
    if (minspace(S) > L.t_ligne){ //Colorie la ligne si un seul choix possible
      int j=0;
      int k=0;
      while(j < S.t_seq){
	int n=S.seq[j];
	for (int i=0 ; i < n ; i++){
	  L.ligne[k+i]='1';
	}
	L=color(k+n,L,'0');
	k=k+n+1;
	j=j+1;
      }
    }
    if (S.seq[0] == 0){ 
      for(int i=0 ; i < L.t_ligne ; i++){
	L=color(i,L,'0');
      }
    }
    if (contrainte(A)){
      int maxi=max(S);
      if (maxi == L.t_ligne){ 
	for(int i=0 ; i < L.t_ligne ; i++){
	  L=color(i,L,'1');
	}
      }
      else {
	int g=0;
	struct Sequence droite;
	struct Sequence gauche;
	while (maxi != S.seq[g]){
	  gauche.seq[g]=S.seq[g];
	  g++;
	}
	int d = S.t_seq - (g+1); 
	for(int i=0 ; i < d ; i++){
	  droite.seq[i]=S.seq[g+1+i];
	}
	gauche.t_seq=g;
	droite.t_seq=d;
	int pd=minspace(droite);
	int pg=minspace(gauche);
	int pet=L.t_ligne-pg-pd;
	int N=pet-maxi;
	if(N == 0){
	  for(int i=0 ; i < pet ;i++){
	    L=color(pg+i,L,'1');
	  }
	}
	if(N > 0){
	  for(int j=0 ; j < pet-2*N ; j++)
	    L=color(pg+N+j,L,'1');
	}
      }
    }
    return L;
  }
}

/* On s'occupe içi des lignes/colonnes où certaines cases étant déjà pleines, on obtient de nouvelles contraintes sure le reste de la ligne/colonne en fonction des coefficients. On cherche à remplir les extrémités de la ligne/colonne*/

/* Donne les indices des cases coloriées en noire dans la ligne */

struct Sequence nb_noir(struct Asso T)
{ 
  struct Sequence Tab={1,{-1}};// -1 pour dire qu'il n'y a pas de noire sur la ligne
  int compteur = 0;
  int t=0;
  for(int l = 0 ; l < T.Line.t_ligne ; l++)
    {
      if(T.Line.ligne[l] == '1')
	{
	  t++;
	  Tab.seq[compteur] = l;
	  compteur++;
	}
    }
  if (t != 0)
    Tab.t_seq=t;
  return Tab;
}

/* Cas où on a un entier dans la séquence et où la ligne contient 2 cases noires coloriées dont l'interespace est vide */

struct Ligne Remplir (struct Asso T)
{
  struct Sequence Tab=nb_noir(T);
  int i=0;
  int cpt=0;
  while (i < Tab.t_seq){ 
    if (Tab.seq[i+1] - Tab.seq[i] == 1){
      i++;
    }
    else {
      cpt++;
      i++;
    }
  }
  if((T.Seq.t_seq == 1) && (cpt == 2))
    {
      if(T.Seq.seq[0] >= (Tab.seq[Tab.t_seq-1] - Tab.seq[0] + 1))
	{
	  for(int k = Tab.seq[0] ; k <= Tab.seq[Tab.t_seq-1] ; k++)
	    T.Line=color(k,T.Line,'1');
	}
    }
  return T.Line;
}

/*Fonction qui traite le cas où un batonnet commence à se former à gauche et n'est pas complet */

struct Ligne Extrem_left (struct Asso T)
{
  struct Sequence Tab=nb_noir(T);
  int start=0;
  int n=0;
  if (Tab.seq[0] != -1){
    while (T.Line.ligne[start] == '0')
      start++;
    
    if(Tab.seq[n] == start){
      for(int i = 0 ; i < T.Seq.seq[n] ; i++)
	T.Line=color(start+i,T.Line,'1');
      
      if(start+T.Seq.seq[n] < T.Line.t_ligne){
	T.Line=color(start+T.Seq.seq[n],T.Line,'0');
	start=start+T.Seq.seq[n];
	n++;
      }
    }
  }
  return T.Line;
}

/*Fonction qui traite le cas où un batonnet commence à se former à droite et n'est pas complet */

struct Ligne Extrem_right (struct Asso T)
{
  struct Sequence Tab=nb_noir(T);
  int end=T.Line.t_ligne-1;
  int n=T.Seq.t_seq-1;
  int f=Tab.t_seq-1;
  if (Tab.seq[0] != -1){
    while (T.Line.ligne[end] == '0')
      end--;
    if(Tab.seq[f] == end){
      for(int i = 0 ; i < T.Seq.seq[n]  ; i++)
	T.Line=color(end-(T.Seq.seq[n]-1)+i,T.Line,'1');
      
      if (end-T.Seq.seq[n] >= 0){
	T.Line=color(end-T.Seq.seq[n],T.Line,'0');
	end=end-T.Seq.seq[n];
	n--;
	f--;
      }
    }
  }
  return T.Line;
}

/* Fonction qui remplie les premiers espaces lorsqu'un batonnet à commencé à se former après les espaces : cas  _ _ _ 1 1 0 */

struct Ligne first_left_space (struct Asso T){
  struct Ligne L=T.Line;
  struct Sequence S=T.Seq;
  int i=0;
  int cpt=0;
  int space=0;
  int black=0;
  int N=S.seq[0]; 
  while (cpt == 0){
    if (L.ligne[i] == '_'){
      space=nbl_consec_space(L,i);
      if (L.ligne[i+space] == '0' && space < N ){ 
	for (int k=0; k < space;k++)
	  L=color(k+i,L,'0');
      }

      if (L.ligne[i+space] == '1'){
	black=nbl_consec_noire(L,i+space);
	if (L.ligne[i+space+black] == '0' && N+1 > space){ 
	  i=i+space+black-1;
	  for (int k=0; k < N; k++){
	    L=color(i,L,'1');
	    i--;
	  }
	  while (i >= 0){
	    L=color(i,L,'0');
	    i--;
	  }
	}
      }
      cpt++;
    }
    else cpt++; 
  }
  return L;
}

/* Fonction qui remplie les derniers espaces lorsqu'un batonnet à commencé à se former à droite après les espaces : cas  0 1 1 _ _ _ */

struct Ligne last_right_space (struct Asso T){
  struct Ligne L=T.Line;
  struct Sequence S=T.Seq;
  int i=L.t_ligne-1;;
  int cpt=0;
  int space=0;
  int black=0;
  int N=S.seq[S.t_seq-1];
  while (cpt == 0){
    if (L.ligne[i] == '_'){
      space=nbr_consec_space(L,i);
      if (L.ligne[i-space] == '0' && space < N ){ 
	for (int k=0; k < space;k++)
	  L=color(i-k,L,'0');
      }

      if (L.ligne[i-space] == '1'){
	black=nbr_consec_noire(L,i-space);
	if (L.ligne[i-space-black] == '0' && N+1 > space){ 
	  i=i-space-black+1;
	  for (int k=0; k < N; k++){
	    L=color(i,L,'1');
	    i++;
	  }
	  while (i < L.t_ligne){
	    L=color(i,L,'0');
	    i++;
	  }
	}
      }
      cpt++;
    }
    else cpt++; 
  }
  return L;
}

/* On rempli lorsque c'est possible les premières cases en blanc */

/*Créer la séquence des 2 indices des extrémités du premier bâtonnet non formé visible sur la ligne */

struct Sequence First_Block (struct Asso T)
{

  struct Sequence Block;
  struct Sequence Start;
  Start.t_seq=0;
  Block.t_seq=2;

  for(int i = 0; i < T.Line.t_ligne ; i++)
    {
      if(T.Line.ligne[i] == '1')
	{
	  Start.t_seq++;
	  Start.seq[Start.t_seq - 1]=i;
	}
    }
  if(Start.t_seq == 0)
    {
      Start.t_seq=1;
      Start.seq[0]=0;
    }
  Block.seq[0]=Start.seq[0];
  Block.seq[1]=Block.seq[0];
  for(int l = 1; l < Start.t_seq ; l++)
    {
      if((Start.seq[l]-Start.seq[0])<=l)
	 Block.seq[1]=Start.seq[l];
    }

  return Block;
}

/*Créer la séquence des 2 indices des extrémités du dernier bâtonnet non formé visible sur la ligne */

struct Sequence Last_Block (struct Asso T)
{

  struct Sequence Block;
  struct Sequence Start;
  Start.t_seq=0;
  Block.t_seq=2;

  for(int i = 0 ; i < T.Line.t_ligne ; i++)
    {
      if(T.Line.ligne[i] == '1')
        {
          Start.t_seq++;
          Start.seq[Start.t_seq - 1]=i;
        }
    }
  if(Start.t_seq == 0)
    {
      Start.t_seq=1;
      Start.seq[0]=0;
    }
  Block.seq[1]=Start.seq[Start.t_seq - 1];
  Block.seq[0]=Block.seq[1];
  for(int l = 1 ; l < Start.t_seq ; l++)
    {
      if((Start.seq[Start.t_seq - 1]-Start.seq[Start.t_seq - 1 - l])<=l)
	Block.seq[0]=Start.seq[Start.t_seq - 1 - l];
    }
 
  return Block;
}

/* Cette fonction détermine les cases à colorier en blanc sur les premiers espaces (à gauche) */

struct Ligne First_Space (struct Asso T)
  {
    struct Sequence Block=First_Block(T);
    if(Block.seq[0]<T.Seq.seq[0]+1)
      {
	int pire = Block.seq[1] - T.Seq.seq[0] + 1;
	for(int i = 0 ; i < pire ; i++)
	  T.Line=color(i,T.Line,'0');
      }
    return T.Line;
  }

/* Cette fonction détermine les cases à colorier en blanc sur les derniers espaces (à droite) */

struct Ligne Last_Space (struct Asso T)
{
  struct Sequence Block=Last_Block(T);
  if(((T.Line.t_ligne-1-Block.seq[1])<T.Seq.seq[T.Seq.t_seq-1]+1)&&(T.Seq.t_seq > 1))
    {
      int pire = T.Seq.seq[T.Seq.t_seq-1] + Block.seq[0] - 1;
      for(int i = T.Line.t_ligne - 1 ; i > pire ; i--)
	T.Line=color(i,T.Line,'0');//T.Line.ligne[i]='0';
    }
  return T.Line;
}
