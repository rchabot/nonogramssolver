#define MAX 2500

/*Défintion des structures dont on va se servir par la suite*/

struct Dimension {
  int nb_lig;
  int nb_col;
};

struct Table { // Permet de récupérer à partir de la lecture du fichier .txt une séquence donnée et sa taille */
  int nb_seq;
  int tab_taille[MAX]; // tableau contenant les tailles des séquences
  int nb_elt;
  int tab_elt[MAX]; // tableau contenant les éléments de chaque séquence
};

struct Ligne { // Permet de représenter une ligne de la grille mais également la grille elle-même (sans prendre en compte les séquences d'entier
  int t_ligne;
  char ligne[MAX];
};

struct Sequence { //Permet de représenter une séquence d'entier associé à une ligne
  int t_seq;
  int seq[MAX];
};

struct Asso { // on va utilisé cette strucure pour associé une ligne et sa séquence d'entier
  struct Ligne Line;
  struct Sequence Seq;
};

struct Ensemble { //Permet de représenter l'ensemble des éléments composant une grille
  struct Dimension dim; 
  struct Table seq_ligne;
  struct Table seq_col;
  struct Ligne grille; 
};
