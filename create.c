#include <time.h>
#include "struct.h"
#include "toolbox.h"

/*Fonction pour supprimer N éléments aléatoirement dans une grille */

struct Ensemble Delete(struct Ensemble GRID, int N){
  srand( time(NULL) );
  int d=GRID.dim.nb_lig*GRID.dim.nb_col;
  for (int i=0; i < N;i++){
    int r=rand()%d;
    if (GRID.grille.ligne[r] != '_')
      GRID.grille.ligne[r]='_';
    else {
      while (GRID.grille.ligne[r] == '_'){
	r=rand()%d;
      }
      GRID.grille.ligne[r]='_';
    }
  }
  return GRID;
}

/* Fonction créant un grille solvable de dimensions l*c contenant N éléments à l'initialisation */

struct Ensemble Create (int l, int c, int N){
  struct Ensemble GRID;
  GRID.dim.nb_lig=l;
  GRID.dim.nb_col=c;
  GRID.grille.t_ligne=l*c;
  srand ( time(NULL) );
  for(int i = 0 ; i < l*c ; i++)
    GRID.grille.ligne[i]=(48+rand()%2);
  
  int nb_elt_lig=0;
  int nb_elt_col=0;
  for(int i=1; i < l+1;i++){
    int Tab[c]; // Tableau qui va contenir la longueur des batonnets pour les lignes
    int curseur=0;
    int cpt=0;
    int nb_coeff=0;
    int taille_seq=0;
    struct Ligne L=getLINE(i,GRID);
    while (curseur < c){
      Tab[curseur]=0;
      if (L.ligne[curseur]=='1'){
	cpt++;
	if (curseur == (c -1))
	  Tab[curseur]=cpt;
	curseur++;
      }
      if (L.ligne[curseur]=='0'){
	Tab[curseur]=cpt;
	cpt=0;
	curseur++;
      }
    }
    curseur=0;
    while (curseur < c){
      if (Tab[curseur]){
	GRID.seq_ligne.tab_elt[nb_elt_lig+nb_coeff]=Tab[curseur];
	nb_coeff++;
	taille_seq++;
	curseur++;
      }
      else curseur++;
    }
    if (nb_coeff==0){ //i.e. on a que des 0 dans la ligne
      GRID.seq_ligne.tab_elt[nb_elt_lig+1]=0;
      nb_elt_lig++;
      GRID.seq_ligne.tab_taille[i-1]=1;
    }
    else{
      GRID.seq_ligne.tab_taille[i-1]=taille_seq;
      nb_elt_lig=nb_elt_lig+nb_coeff;
    }
  }
  GRID.seq_ligne.nb_seq=l;
  GRID.seq_ligne.nb_elt=nb_elt_lig;
  for(int j=1; j < c+1;j++){
    int Tab[l]; // Tableau qui va contenir la longueur des batonnets pour les colonnes
    int curseur=0;
    int cpt=0;
    int nb_coeff=0;
    int taille_seq=0;
    struct Ligne C=getCOL(j,GRID);
    while (curseur < l){
      Tab[curseur]=0;
      if (C.ligne[curseur]=='1'){
	cpt++;
	if (curseur == (l -1))
	  Tab[curseur]=cpt;
	curseur++;
      }
      if (C.ligne[curseur]=='0'){
	Tab[curseur]=cpt;
	cpt=0;
	curseur++;
      }
    }
    curseur=0;
    while (curseur < l){
      if (Tab[curseur]){
	GRID.seq_col.tab_elt[nb_elt_col+nb_coeff]=Tab[curseur];
	nb_coeff++;
	taille_seq++;
	curseur++;
      }
      else curseur++;
    }
    if (nb_coeff==0){ // on a que des 0 dans la ligne
      GRID.seq_col.tab_elt[nb_elt_col+nb_coeff]=0;
      nb_elt_col++;
      GRID.seq_col.tab_taille[j-1]=1;
    }
    else{
      GRID.seq_col.tab_taille[j-1]=taille_seq;
      nb_elt_col=nb_elt_col+nb_coeff;
    }
    GRID.seq_col.nb_seq=c;
    GRID.seq_col.nb_elt=nb_elt_col;
  }
  GRID=Delete(GRID,l*c-N);
  return GRID;
}

