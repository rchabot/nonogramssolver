#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "toolbox.h"

/* Prend la ligne N et l'ajoute dans la grille */

struct Ensemble put_line_ingrid(int N, struct Ligne LIGNE, struct Ensemble GRID){
  for (int i=0; i < LIGNE.t_ligne;i++){
    GRID.grille.ligne[GRID.dim.nb_col*(N-1)+i]=LIGNE.ligne[i]; 
    }
  return GRID;   
}

/* Prend la colonne N et l'ajoute dans la grille */

struct Ensemble put_col_ingrid(int N, struct Ligne LIGNE, struct Ensemble GRID){
  for (int j=0; j < LIGNE.t_ligne;j++){
    GRID.grille.ligne[(N-1)+GRID.dim.nb_col*j]=LIGNE.ligne[j]; 
    }
  return GRID;   
}

/* Compte le nombre de batonnets formés en début de ligne, on compte de gauche à droite */

int left_count(struct Ligne L){
  int i=0;
  int nb_bat=0;
  while (L.ligne[i] != '_' && i < L.t_ligne){
    if (L.ligne[i] == '0')
      i++;
    if (L.ligne[i] == '1'){
      while (L.ligne[i] == '1'){
	i++;
      }
      if (L.ligne[i] == '0')
	nb_bat++;
    }
  }
  return nb_bat;
}
       
/* Compte le nombre de batonnets formés en fin de ligne, on compte de droite à gauche */

int right_count(struct Ligne L){
  int i=L.t_ligne - 1;
  int nb_bat=0;
  while (L.ligne[i] != '_' && i >= 0){
    if (L.ligne[i] == '0')
      i--;
    if (L.ligne[i] == '1'){
      while (L.ligne[i] == '1' && i >= 0){
	i--;
      }
      if (L.ligne[i] == '0')
	nb_bat++;
    }
  }
  return nb_bat;
}

/*Compte le nombre de block de cases indéterminées et noire, évalue leur longueur et retourne le résultat dans un tableau*/

struct Sequence count_blocks(struct Asso T){
  struct Sequence S=T.Seq;
  struct Ligne L=T.Line;
  
  struct Sequence tab;

  int i=0;
  int N=0;
  
  while (i < L.t_ligne){
    if (L.ligne[i] == '_'){
      int l=0;
      while (L.ligne[i] != '0' && i < L.t_ligne){
	i++;
	l++;
      }
      tab.seq[N]=l;
      N++;
    }
    else i++;
  }
  tab.t_seq=N;
  return tab;
}

/* Extrait la partie centrale de la ligne en enlevant les batonnets consécutifs formées en début et fin de ligne */

struct Asso Extract(struct Asso LIGNE){
  struct Ligne droite;
  struct Ligne L=LIGNE.Line;
  struct Sequence S=LIGNE.Seq;
  struct Asso tmp;
  int nb_bat_left=left_count(L);
  int nb_bat_right=right_count(L);
  int l1=0;
  int l=0;
  int d=0;
  int Nc;
  if (!Complete(L)){
    while (l1 == 0){ 
      if (L.ligne[d] == '0')
	d++;
      if (L.ligne[d] == '1'){
	Nc=nbl_consec_noire(L,d);
	if (L.ligne[Nc+d] == '0')
	  d=d+Nc;
	else {
	  while (d < L.t_ligne){ 
	    droite.ligne[l1]=L.ligne[d];
	    l1++;
	    d++;
	  }
	}
      }
      if (L.ligne[d] == '_'){
	while (d < L.t_ligne){ 
	  droite.ligne[l1]=L.ligne[d];
	  l1++;
	  d++;
	}
      }
    }
    droite.t_ligne=l1; 
    int f=droite.t_ligne-1; 
    char gauche[50];
    while (l == 0){ 
      if (droite.ligne[f] == '0')
	f--;
      if (droite.ligne[f] == '1'){
	Nc=nbr_consec_noire(droite,f);
	if (droite.ligne[f-Nc] == '0')
	  f=f-Nc;
	else {
	  while (f >= 0){ 
	    gauche[l]=droite.ligne[f];
	    l++;
	    f--;
	  }
	}
      }
      if (droite.ligne[f] == '_'){
	while (f >=0){ 
	  gauche[l]=droite.ligne[f];
	  l++;
	  f--;
	}
      }
    }
    tmp.Line.t_ligne=l;
    for (int k=0; k < l; k++){
      tmp.Line.ligne[k]=gauche[l-k-1];
    }
    int t=S.t_seq - nb_bat_left - nb_bat_right;
    if (t == 0 ){ 
      tmp.Seq.t_seq=1;
      tmp.Seq.seq[0]=0;
      return tmp;
    }
    else{
      for(int j=0; j < t; j++)
	tmp.Seq.seq[j]=S.seq[nb_bat_left+j];
      tmp.Seq.t_seq=t;
      return tmp;
    }
  }
  else return LIGNE;
}

/* Prend une ligne extraite (ayant été modifiée ou non) puis la remet dans la grille */

struct Ensemble put_extracted_line(int N, struct Ligne L, struct Ensemble GRID){
  struct Ligne INIT=getLINE(N,GRID);
  int l=0;
  int d=0;
  int Nc;    
  if (!Complete(INIT)){
    while (l == 0){ 
      if (INIT.ligne[d] == '0')
	d++;
      if (INIT.ligne[d] == '1'){
	Nc=nbl_consec_noire(INIT,d);
	if (INIT.ligne[Nc+d] == '0'){
	  d=d+Nc+1;
	}
	else {
	  while (l < L.t_ligne){ 
	    INIT.ligne[d]=L.ligne[l];
	    l++;
	    d++;
	  }
	}
      }
      if (INIT.ligne[d] == '_'){
	while (l < L.t_ligne){ 
	  INIT.ligne[d]=L.ligne[l];
	  l++;
	  d++;
	}
      }
    }
  }
  return put_line_ingrid(N,INIT,GRID);
}

/* Idem avec les colonnes */

struct Ensemble put_extracted_col(int N, struct Ligne L, struct Ensemble GRID){
  struct Ligne INIT=getCOL(N,GRID);
  int l=0;
  int d=0;
  int Nc;
  if (Complete(INIT) == 0){
    while (l == 0){ 
      if (INIT.ligne[d] == '0')
	d++;
      if (INIT.ligne[d] == '1'){
	Nc=nbl_consec_noire(INIT,d);
	if (INIT.ligne[Nc+d] == '0')
	  d=d+Nc+1;
	else {
	  while (l < L.t_ligne){ 
	    INIT.ligne[d]=L.ligne[l];
	    l++;
	    d++;
	  }
	}
      }
      if (INIT.ligne[d] == '_'){
	while (l < L.t_ligne){ 
	  INIT.ligne[d]=L.ligne[l];
	  l++;
	  d++;
	}
      }
    }
  }
  return put_col_ingrid(N,INIT,GRID);
}
