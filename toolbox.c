#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "struct.h"

/* Caractère qu'un case peut contenir */

int alphabet(int n){
  switch(n){
  case 48:
    return 1;
    break;
  case 49:
    return 1;
    break;
  case 95:
    return 1;
    break;
  default:
    return 0;
    break;
  }
}

/* Fonction qui vérifie qu'on colorie bien une case de la même couleur que sa couleur initiale lorsque celle-ci n'est pas indéterminée */

struct Ligne color(int i, struct Ligne L, int c){ // i numéro de la case à colorier dans la ligne et c caractère à insérer
  if (c==48){
    if (L.ligne[i]=='0' || L.ligne[i]=='_')
      L.ligne[i]='0';
    return L;
  }
  if (c==49){
    if (L.ligne[i]=='1' || L.ligne[i]=='_')
      L.ligne[i]='1';
    return L;
  }
  else
    return L;
}
 
/* Fonction pour afficher une ligne de la grille */

void print_str(struct Ligne L){
  int j=0;
  while(j < L.t_ligne){
    printf("%3c", L.ligne[j]);
    j++;
  }
  printf("\n");
}

/* Fonction pour afficher une séquence associée à une grille */

void print_tab(struct Sequence S){
  int i = 0;
  while(i < S.t_seq) {
    printf("%3d ", S.seq[i]); 
    i = i + 1;
  }
  printf("\n");
}

/* Fonction qui convertit la table contenant la taille des séquences en un struct Sequence, on a en sortie un tableau conteant les tailles des séquences */

struct Sequence TtoS_taille(struct Table table){
  struct Sequence taille_seq;
  taille_seq.t_seq=table.nb_seq;
  for(int i=0; i<taille_seq.t_seq;i++)
    taille_seq.seq[i]=table.tab_taille[i];
  return taille_seq;
}

/* Fonction qui convertit le tableau contenant les éléments des séquences en un struct Sequence, on a en sortie un tableau contenant les entiers des séquentes mis successivement les uns à la suite des autres */

struct Sequence TtoS_elt(struct Table table){
  struct Sequence tab_elt;
  tab_elt.t_seq=table.nb_elt;
  for(int i=0; i<tab_elt.t_seq ;i++)
    tab_elt.seq[i]=table.tab_elt[i];
  return tab_elt;
}

/* Récupère la séquence (et sa taille) de la séquence associée à la N-ième ligne */

struct Sequence getseq(int N, struct Sequence taille_seq, struct Sequence tab_elt){ //N >= 1
  struct Sequence sequence;
  sequence.t_seq=taille_seq.seq[N-1];
  int nb_elt_ignore=0;
  int i=0;
  while (i < N-1){
    nb_elt_ignore=nb_elt_ignore+taille_seq.seq[i];
    i++;
  }
  for (int j=0; j < sequence.t_seq; j++)
    sequence.seq[j]=tab_elt.seq[nb_elt_ignore+j];
  return sequence;
}

/* Récupère la N-ième ligne de la grille (en comptant à partir de 1) */

struct Ligne getLINE(int N, struct Ensemble GRID){
  int c=GRID.dim.nb_col;
  struct Ligne LIGNE;
  LIGNE.t_ligne=c;
  for (int i=0; i < c; i++)
    LIGNE.ligne[i]=GRID.grille.ligne[(N-1)*c+i];
  return LIGNE;
}

/* Récupère N-ième colonne */

struct Ligne getCOL(int N, struct Ensemble GRID){
  int l=GRID.dim.nb_lig;
  int c=GRID.dim.nb_col;
  struct Ligne COL;
  COL.t_ligne=l;
  for (int j=0; j < l; j++)
    COL.ligne[j]=GRID.grille.ligne[(N-1)+j*c];
  return COL;
}

/* Récupère la N-ième ligne et la séquence d'entier qui lui est associée */

struct Asso associerseq_line(int N, struct Ensemble GRID){
  struct Asso LIGNE_N;
  LIGNE_N.Line=getLINE(N,GRID);
  LIGNE_N.Seq=getseq(N,TtoS_taille(GRID.seq_ligne),TtoS_elt(GRID.seq_ligne));
  return LIGNE_N;
}

/* Récupère la N-ième colonne et la séquence d'entier qui lui est associée */

struct Asso associerseq_col(int N, struct Ensemble GRID){
  struct Asso COL_N;
  COL_N.Line=getCOL(N,GRID);
  COL_N.Seq=getseq(N,TtoS_taille(GRID.seq_col),TtoS_elt(GRID.seq_col));
  return COL_N;
}

/*On détermine la place minimum que peut prendre une sequence d'entiers sur une ligne, en supposant que l'on démarre sur un bord et que l'on fini par une case blanche */

int minspace(struct Sequence sequence){
  int p=0;
  int i=0;
  while (i < sequence.t_seq){
    p=p+sequence.seq[i]+1;
    i=i+1;
  }
  return p;
}

/* Fonction déterminant le maximum d'une séquence d'entiers */

int max(struct Sequence sequence){ 
  int maxi=0;
  if (sequence.seq[0] == 0)
    return maxi;
  else{
  for(int i=0 ; i < sequence.t_seq ; i++){
    if (sequence.seq[i] > maxi){
    maxi=sequence.seq[i];
    }
  }
  }
  return maxi;
}

/* Compte le nombre d'espaces sur une ligne vers la gauche depuis curseur */

int nbl_consec_space(struct Ligne L, int curseur){
  int n=0;
  while (L.ligne[curseur+n] == '_')
    n++;
  return n;
}

/* Compte le nombre d'espaces sur une ligne vers la droite depuis curseur */

int nbr_consec_space(struct Ligne L, int curseur){
  int i=curseur;
  int n=0;
  while (L.ligne[i] == '_'){
    n++;
    i--;
  }
  return n;
}


/* Compte le nombre de case coliriée en noire consécutivement de gauche à droite */

int nbl_consec_noire(struct Ligne L, int curseur){
  int n=0;
  while (L.ligne[curseur+n] == '1')
    n++;
  return n;
}

/* Compte le nombre de case coliriée en noire consécutivement de droite à gauche */

int nbr_consec_noire(struct Ligne L, int curseur){
  int i=curseur;
  int n=0;
  while (L.ligne[i] == '1'){
    n++;
    i--;
  }
  return n;
}

/* Détermine si une ligne est complète ou non */

int Complete(struct Ligne L)
{
  int c = 0;
  for(int i = 0 ; i < L.t_ligne ; i++)
    {
      if(L.ligne[i] != '_')
	c++;
    }
  if(c == L.t_ligne)
    return 1;
  else
  return 0;
}

/* Fonction qui détermine si une ligne est résolue ou non */

int isSolve(struct Asso T){
  struct Ligne L=T.Line;
  struct Sequence S=T.Seq;
  int i=0;
  int N=0;
  if (S.seq[0] == 0){
    while (i < L.t_ligne){
      if (L.ligne[i] == '1')
	return 0;
      i++;
    }
  }
  if (S.seq[0] == L.t_ligne){
    while (i < L.t_ligne){
      if (L.ligne[i] == '0')
	return 0;
      i++;
    }
  }
  else {
    while (i < L.t_ligne){
      if (L.ligne[i] == '0')
	i++;
      if (L.ligne[i] == '1'){
	int black=0;
	while (L.ligne[i] == '1'){
	  i++;
	  black++;
	}
	if (black == S.seq[N])
	  N++;
	else return 0;
      }
    }
  }
  return 1;
}

/* Fonction qui détermine si une grille est valide (solution) ou non */

int isValide(struct Ensemble GRID){
  int N=0;
  for (int i=1; i < GRID.dim.nb_lig+1; i++){
    if (isSolve(associerseq_line(i,GRID)))
      N++;
  }
  for (int j=1; j < GRID.dim.nb_col+1; j++){
    if (isSolve(associerseq_col(j,GRID)))
      N++;
  }
  if (N == (GRID.dim.nb_lig + GRID.dim.nb_col))
    return 1;
  else return 0;
}
