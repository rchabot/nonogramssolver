int chiffre(int n);
int longueur_nombre(char *str, int curseur);
int convertir_nombre(char *str, int curseur);
struct Dimension convertir_DIM(char *str);
int compter_nombres(char *str,int curseur);
int compter_pointvirg(char *str);
struct Table convertir_LIGNE(char *str);
struct Ensemble inserer_ligne_fichier(char *str,int n, struct Ensemble GRID);
struct Ensemble convertir_fichier(char *str);
