#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "struct.h"
#include "toolbox.h"
#include "create.h"
#include "colorier.h"
#include "lecturefichier.h"
#include "extraction.h"
#include "fonctionstest.h"

/* Couple contenant la grille et sa solvabilité, qui sera déterminée par le solveur */

struct GSolv {
  struct Ensemble GRID;
  int solv;
};

/* Fonction permettant de remplir à coup sûr une grille par ses extrémités */

struct Ligne full_extremes(struct Asso L){
  L.Line=Remplir(L);
  L.Line=First_Space(L);
  L.Line=Last_Space(L);
  L.Line=Extrem_left(L);
  L.Line=Extrem_right(L);
  L.Line=first_left_space(L);
  L.Line=last_right_space(L);
  return L.Line;
}

/*Fonction qui permet d'imposer la couleur BLANC ou NOIR dans la case i de la grille */

struct Ensemble Impose(int i,struct Ensemble GRID, int c){
  GRID.grille.ligne[i]=c;
  return GRID;
}

/* Fonction qui change la couleur d'une case dans une grille */

struct Ensemble Change(int i,struct Ensemble GRID){
  if (GRID.grille.ligne[i] == '1'){
    GRID.grille.ligne[i]='0';
  }
  if (GRID.grille.ligne[i] == '0'){
    GRID.grille.ligne[i]='1';
  }
  return GRID;
}

/* Fonction qui résoud partiellement une ligne de la grille */

struct Ensemble modifier_ligne(struct Ensemble GRID){
  struct Asso L;
  for (int i=1; i < GRID.dim.nb_lig+1;i++){
    L=Extract(associerseq_line(i,GRID));
    L.Line=boxe(L);
    L.Line=full_extremes(L);
    GRID=put_extracted_line(i,L.Line,GRID);
  }
  return GRID;
}

/* Fonction qui résoud partiellement une colonne de la grille */

struct Ensemble modifier_col(struct Ensemble GRID){
  struct Asso L;
  for (int i=1; i < GRID.dim.nb_col+1;i++){
    L=Extract(associerseq_col(i,GRID));
    L.Line=boxe(L);
    L.Line=full_extremes(L);
    GRID=put_extracted_col(i,L.Line,GRID);
  }
  return GRID;
}

/* Fonction qui permet de tester l'égalité de 2 grilles. On ne prend pas en compte l'égalité des séquences d'entier pour chaque ligne et chaque colonne car ce n'est pas nécessaire pour notre utilisation de cette fonction */

int Equal(struct Ensemble G, struct Ensemble H){
  int dimG=G.dim.nb_lig*G.dim.nb_col;
  int dimH=H.dim.nb_lig*H.dim.nb_col;
  assert(dimG == dimH);
  int N=0;//Compte le nombre d'éléments égaux
  for (int i=0; i < dimG; i++){
      if (G.grille.ligne[i] == H.grille.ligne[i])
	N++;
  }
  if ( N == dimG )
    return 1;
  else return 0;
} 

/* Fonction qui cherche la prochaine case indéterminée à partir de l'élément n */

int next_void(struct Ligne grille, int n){ 
  int i=0;
  while (grille.ligne[n+i] != '_' && i < grille.t_ligne)
    i++;
  return n+i;
}

/* Fonction qui tente de résoudre une grille par itération. Si I(G)=G alors la fonction s'arrête et renvoit G */

struct Ensemble I(struct Ensemble GRID){
  struct Ensemble H=modifier_ligne(GRID);
  int NBLIG=GRID.dim.nb_lig;
  int cpt=0;
  while (!Equal(H,GRID)){
    H=GRID;
    GRID=modifier_ligne(GRID);
    GRID=modifier_col(GRID);
    printf("\n Nombre d'itérations : %d \n",cpt);
    cpt++;
    for (int k=1; k < NBLIG+1;k++){
      print_str(getLINE(k,GRID));
    }
  }
  return GRID;
}

/* Fonction de résolution */

struct GSolv Solve(struct GSolv GS, int *CA, int *CO,struct Ligne *T,int *p, int *A){
  struct Ensemble G=GS.GRID;
  int NBLIG=G.dim.nb_lig;
  int NBCOL=G.dim.nb_col;
  int t=*p;
  printf("Appel numéro: %d et t= %d \n",*A,*p);
  (*A)++;
  printf("Grille en entrée dans le nouvel appel de Solvabilité \n");
  for (int i=1; i < NBLIG+1;i++){
    print_str(getLINE(i,G));
  }
  struct Ensemble H=I(G);//On calcule par itération
  
  printf("Grille H=Iterative(G) \n");
  for (int i=1; i < NBLIG+1;i++){
    print_str(getLINE(i,H));
  }
  if (Complete(H.grille)){
    printf("Cas grille H Complète \n");
    printf("isValide(H)= %d \n",isValide(H));
    if (isValide(H)){
      printf("Grille H Complète et Valide donc G est solvable \n");
      GS.GRID=H;
      GS.solv=1;
      return GS;
    }
    else {
      printf("Cas grille H non valide et complète \n");
      if (*(CO + *p-1) == '0'){ 
	printf("Cas où '0' testé sur la case %d de la grille précédente ET échec \n",*(CA+*p-1));//échec = pas de solutions trouvées
	*(CO+*p-1)='1';
	GS.GRID.grille=*(T+*p-1); //On reprend la grille qu'on avait avant de tester le '0'
	GS.GRID=Impose(*(CA+*p-1),GS.GRID,*(CO+*p-1));// On test en mettant cette case à '1'
	return GS;
      }
      if (*(CO+*p-1) == '1'){ 
	printf("Cas où '1' testé sur la case %d de la grille précédente ET échec \n",*(CA+*p-1));
	while (*(CO+*p-1) == '1' && *p >= 0){ //On remonte jusqu'à trouver une case testée à '0'
	  (*p)--;
	  printf("Retour au test t= %d \n",*p);
	}
	if (*p == 1 && *(CO+*p) == '1'){
	  GS.GRID.grille=*(T+0);
	  printf("La grille est non solvable \n");
	  GS.solv=0;
	  return GS;
	}
	if (*p == 1 && *(CO+*p) == '0'){
	  printf("L'hypothèse case %d est '0' est fausse. On test '1' dans la case %d \n",*(CA+*p),*(CA+*p));
	  *(CO+*p)='1';
	  GS.GRID.grille=*(T+1);
	  GS.GRID=Impose(*(CA+*p),GS.GRID,*(CO+*p)); //On impose '1' dans la grille enregistrée précédemment
	  return GS;
	}
	else { // t > 1
	  printf("t= %d on va tester '1' dans la case %d \n",*p,*(CA+*p));
	  *(CO+*p-1)='1';
	  GS.GRID.grille=*(T+*p-1);
	  GS.GRID=Impose(*(CA+*p-1),GS.GRID,*(CO+*p-1));
	  return GS;
	}
      }
      if (*p == 1){
	GS.GRID.grille=*(T+0);
	printf("La grille est non solvable \n");
	GS.solv=0;
	return GS;
      }
    }
  }
  else {
    printf("Grille H non complète \n");
    if (*p == 1 && *(CA+(*p-1)) == -1){ //Cas initialisation avant premier appel récursif
      *(CA+*p)=next_void(H.grille,0);
      printf("Cas t=1, On met %d dans CASE[%d] et on stocke H dans T[%d]\n",next_void(H.grille,0),*p,*p);
      *(CO+*p)='0';
      *(T+*p)=H.grille;//On stocke la grille avant modification
      GS.GRID=Impose(*(CA+*p),H,*(CO+*p));
      (*p)++;
      return GS;
    }
    else {
      *(T+*p)=H.grille;//On stocke grille en sortie d'itération sur laquelle on va faire l'appel récursif
      *(CA+*p)=next_void(H.grille,*(CA+(*p-1)));//On cherche prochain '_'
      printf("Cas t>1, On met %d dans CASE[%d] et on stocke H dans T[%d]\n",next_void(H.grille,*(CA+*p-1)),*p,*p);
      *(CO+*p)='0'; 
      GS.GRID=Impose(*(CA+*p),H,*(CO+*p));//On met '0' dans la case cherchée précédemment
      (*p)++;
      return GS;
    }
  }
}  

/* Fonction qui résoud le problème de solvabilité */

struct GSolv Solvabilite(struct GSolv GS){
  int CA[MAX];
  int CO[MAX];
  int t=0;
  struct Ligne T[2600];
  *(CA + 0)=-1;
  *(CO + 0)='_';
  *(T + 0)=GS.GRID.grille;
  t=1;
  int A=0;
  while (GS.solv != 0 && GS.solv != 1){
    GS=Solve(GS,&CA,&CO,&T,&t,&A);
  }
  return GS;
  printf("Solvabilité %d \n",GS.solv);
}

/*Fonction qui test l'unicité d'une solution */
/*
int isUnique(struct GSolv GS){
  if (GS.solv == 0){
    printf("Grille non solvable \n");
    return 0;
  }
  else {
    struct GSolv Tmp=GS;
    struct Ensemble GRID=GS.GRID;
    int autre_sol=0;
    int case_modifie=0;
    while (!autre_sol && case_modifie < GS.GRID.grille.t_ligne){
      for (int i=0; i < GRID.grille.t_ligne;i++){
	Tmp.solv=42;
	Tmp.GRID.grille.ligne[i]='_';// On prend la grille qu'on initialise comme grille vide
      }
      printf("case modifié %d et contient %c \n",case_modifie,GRID.grille.ligne[case_modifie]);
      Tmp.GRID.grille.ligne[case_modifie]=GRID.grille.ligne[case_modifie];
      Tmp.GRID=Change(case_modifie,Tmp.GRID);
      Tmp=Solvabilite(Tmp);
      printf("Tmp.solv %d \n",Tmp.solv);
      if (Tmp.solv)
	autre_sol++;
      
      case_modifie++;
    }
    if (autre_sol == 0)
      return 1;
    else return 0;     
  }
}
*/

int main(int argc,char *argv[]){
  struct GSolv GS;
  GS.solv=42;
  
  if (argc == 2)
    GS.GRID=convertir_fichier(argv[1]);
  
  else {
    int l;
    int c;
    int N;
    printf("Veuillez entrer le nombre de lignes souhaitées: \n");
    scanf("%2d",&l);
    printf("Veuillez entrer le nombre de colonnes souhaitées: \n");
    scanf("%2d",&c);
    printf("Veuillez entrer le nombre de case coloriée initialement: \n");
    scanf("%4d",&N);
    GS.GRID=Create(l,c,N);
  }
  struct GSolv H=Solvabilite(GS);
  struct Ensemble S=H.GRID;

  int NBLIG=S.dim.nb_lig;
  int NBCOL=S.dim.nb_col;
  printf("Solution proposée \n");
  for (int i=1; i < NBLIG+1;i++){
    print_str(getLINE(i,S));
  }
  
  //Vérification manuelle par l'utilisateur
  /*
  printf("vérifier lignes\n");
  for (int i=1; i < NBLIG+1;i++){
    print_tab(associerseq_line(i,S).Seq);
    print_str(getLINE(i,S));
  }
  printf("Vérifier col\n");
  for (int i=1; i < NBCOL+1;i++){
    print_tab(associerseq_col(i,S).Seq);  
    print_str(getCOL(i,S));  
  }
  */
  
  //Test de l'unicité (comporte un bug car repropose la solution testée)
  /*
  int u=isUnique(H);
  if (!u)
    printf("La grille admet au moins une autre solution \n");
  else printf("La grille admet une unique solution\n");
  */
}
