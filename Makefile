CC=gcc -std=c99
CFLAGS= -Wall -Wextra 
LDFLAGS=
EXEC=solve

all: $(EXEC)
solve : toolbox.o lecturefichier.o create.o  extraction.o colorier.o solveur.o 
	$(CC) -o solve toolbox.o lecturefichier.o create.o extraction.o colorier.o solveur.o $(LDFLAGS)
toolbox.o : toolbox.c 
	$(CC) -o toolbox.o -c toolbox.c $(CFLAGS)

lecturefichier.o : lecturefichier.c struct.h toolbox.h create.h
	$(CC) -o lecturefichier.o -c lecturefichier.c $(CFLAGS)

create.o : create.c struct.h toolbox.h
	$(CC) -o create.o -c create.c $(CFLAGS)

extration.o : extraction.c struct.h toolbox.h
	$(CC) -o extraction.o -c extraction.c $(CFLAGS)

colorier.o : colorier.c struct.h toolbox.h
	$(CC) -o colorier.o -c colorier.c $(CFLAGS)

fonctionstest.o : fonctionstest.c struct.h toolbox.h colorier.h extraction.h lecturefichier.h 
	$(CC) -o fonctionstest.o -c fonctionstest.c $(CFLAGS)

solveur.o : solveur.c struct.h toolbox.h colorier.h extraction.h lecturefichier.h create.h fonctionstest.h
	$(CC) -o solveur.o -c solveur.c $(CFLAGS)

clean:
	rm *.o
	rm solve
