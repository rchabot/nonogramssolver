#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "struct.h"
#include "toolbox.h"
#include "colorier.h"
#include "lecturefichier.h"
#include "extraction.h"

void test_extract(struct Asso T){
  struct Asso var=Extract(T);
  print_tab(var.Seq);
  print_str(var.Line);
}

void test_full_extremes(struct Asso L){
  printf("\n");
  printf("FirstSpace\n");
  L.Line=First_Space(L);
  print_str(L.Line);
  printf("Last_Space \n");
  L.Line=Last_Space(L);
  print_str(L.Line);
  printf("Extr_left\n");
  L.Line=Extrem_left(L);
  print_str(L.Line);
  printf("Extr_right\n");
  L.Line=Extrem_right(L);
  print_str(L.Line);
  printf("first_left\n");
  L.Line=first_left_space(L);
  print_str(L.Line);
  printf("last_right\n");
  L.Line=last_right_space(L);
  print_str(L.Line);
  printf("\n");
}

void test_convertir_LIGNE(char *str){
  struct Table table=convertir_LIGNE(str);
  print_tab(TtoS_taille(table));
  print_tab(TtoS_elt(table));
}

void afficher_dimensions(struct Ensemble GRID){
  printf("Les dimensions de la grille sont nb_lig= %d et nb_col= %d \n",GRID.dim.nb_lig,GRID.dim.nb_col);
}

void afficher_tables(struct Ensemble GRID){
  struct Sequence table_seq_l=TtoS_taille(GRID.seq_ligne);
  struct Sequence table_elt_l=TtoS_elt(GRID.seq_ligne);
  printf("Tableau contenant les tailles des séquences liées aux lignes: \n");
  print_tab(table_seq_l);
  printf("Tableau des éléments des séquences liées aux lignes:\n");
  print_tab(table_elt_l);
  struct Sequence table_seq_c=TtoS_taille(GRID.seq_col);
  struct Sequence table_elt_c=TtoS_elt(GRID.seq_col);
  printf("Tableau contenant les tailles des séquences liées aux colonnes: \n");
  print_tab(table_seq_c);
  printf("Tableau des éléments des séquences liées aux colonnes:\n");
  print_tab(table_elt_c);
}


void afficher_grille(struct Ensemble GRID){
  printf("Nombre de séquence pour les lignes= %d \n",GRID.seq_ligne.nb_seq);
  printf("Nombre de séquence pour les colonnes= %d \n",GRID.seq_col.nb_seq);
  print_str(GRID.grille);
}

void test_getseq(int N, struct Sequence taille_seq, struct Sequence tab_elt){
  print_tab(getseq(N,taille_seq,tab_elt));
}

void test_getline(int N, struct Ensemble GRID){
  print_str(getLINE(N,GRID));
}

void test_getcol(int N, struct Ensemble GRID){
  print_str(getcol(N,GRID));
}

void test_associerl(int N, struct Ensemble GRID){
  print_tab(associerseq_line(N,GRID).Seq);
  print_str(associerseq_line(N,GRID).Line);
}

void test_associerc(int N, struct Ensemble GRID){
  print_tab(associerseq_col(N,GRID).Seq);
  print_str(associerseq_col(N,GRID).Line);
}
