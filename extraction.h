struct Ensemble put_line_ingrid(int N, struct Ligne LIGNE, struct Ensemble GRID);
struct Ensemble put_col_ingrid(int N, struct Ligne LIGNE, struct Ensemble GRID);
struct Asso Extract(struct Asso LIGNE);
struct Ensemble put_extracted_line(int N, struct Ligne L, struct Ensemble GRID);
struct Ensemble put_extracted_col(int N, struct Ligne L, struct Ensemble GRID);
int left_count(struct Ligne L);
int right_count(struct Ligne L);
struct Sequence count_blocks(struct Asso T);
