---Compilation---

La commande `make' entrée dans le terminal permet de compiler le projet dans son ensemble, l'éxécutable ./solve est créé.
Il est conseillé de faire un make clean au préalable

---Exécution de ./solve---

La tâche effectuée dépend du nombre d'arguments:
	    *Si une grille est spécifiée en argument de ./solve (commande `./solve grille.txt') alors celle-ci est résolue ou non selon sa solvabilité.
	    *Si aucun argument n'est spécifié, une grille solvable sera créée. L'utilisateur devra spécifier les dimensions de la grille et le nombre de cases qu'il souhaite laisser coloriées à l'initialisation. La grille générée sera alors résolue.
	    *La commande `./solve grille.txt > file.txt ' permettra d'écrire chaque étape de la résolution dans un fichier texte.
Pour faire le test sur un grille aléatoire, il faut modifier le fichier dim.txt:
	    	  -Dans la première ligne le nombre de lignes de la grille à créer
		  -Dans la deuxième ligne le nombre de colonnes de la grille à créer
		  -Dans la troisième ligne le nombre de case à colorier à la création
Une commande `./solve < dim.txt > file.txt ' permet de stocker les informations dans le fichier file.txt.	  	  
    	    *Pour vérifier manuellement qu'un solution proposée est bien valide, l'utilisateur pourra décommenter la partie commentée à la fin de de `solveur.c' s'intitulant `Vérification manuelle de l'utilisateur'.
    	    *Il est possible qu'une erreur de segmentation apparaisse dans certains cas. 

---Format de grille.txt---

Lorsqu'une grille est entrée en argument, la lecture du fichier .txt nécessite quelques exigences au niveau du format:
	   *Les deuxième et troisième ligne doivent IMPERATIVEMENT finir par un `;'.
	   *Les lignes suivantes doivent contenir les lignes de la grille que l'on souhaite tester avec les mêmes dimensions que celles imposées dans la première ligne. Celles-ci doivent également finir pas `_' ou `1' ou `0'. Si ce n'est pas le cas la solution proposée peut être incorrecte.
	   
---Fichier solveTEST.txt---
	   *Pour voir ce que fait le solveur lors de la résolution d'une grille, un test a été effectué sur la grille `grilleTEXT.txt' et les étapes de la résolution sont affichées dans le fichier `solveTEST.txt'.
